/* jshint indent: 2 */
/*
 * Indie Toolkit: Creativity on a budget
 */

/**
 * Maximum allowed error
 */
MAX_ERROR = 0.3;

/**
 * Common ISO values
 */
ISO_VALUES = [
  { value: 50, fullStop: true },
  { value: 100, fullStop: true },
  { value: 125, fullStop: false },
  { value: 160, fullStop: false },
  { value: 200, fullStop: true },
  { value: 250, fullStop: false },
  { value: 320, fullStop: false },
  { value: 400, fullStop: true },
  { value: 500, fullStop: false },
  { value: 640, fullStop: false },
  { value: 800, fullStop: true },
  { value: 1000, fullStop: false },
  { value: 1250, fullStop: false },
  { value: 1600, fullStop: true },
  { value: 2000, fullStop: false },
  { value: 2500, fullStop: false },
  { value: 3200, fullStop: true }
];

/**
 * Common f-values
 */
F_VALUES = [
  { value: 1.0, fullStop: true },
  { value: 1.2, fullStop: false },
  { value: 1.4, fullStop: true },
  { value: 1.8, fullStop: false },
  { value: 2.0, fullStop: true },
  { value: 2.8, fullStop: true },
  { value: 3.5, fullStop: false },
  { value: 4.0, fullStop: true },
  { value: 5.6, fullStop: true },
  { value: 8.0, fullStop: true },
  { value: 11.0, fullStop: true },
  { value: 16.0, fullStop: true },
  { value: 22.0, fullStop: true },
  { value: 32.0, fullStop: true }
];

/**
 * Common s-values
 */
S_VALUES = [
  { value: 8, label: "8", fullStop: true },
  { value: 6, label: "6", fullStop: false },
  { value: 5, label: "5", fullStop: false },
  { value: 4, label: "4", fullStop: true },
  { value: 3.2, label: "3.2", fullStop: false },
  { value: 2.5, label: "2.5", fullStop: false },
  { value: 2, label: "2", fullStop: true },
  { value: 1.6, label: "1.6", fullStop: false },
  { value: 1.3, label: "1.3", fullStop: false },
  { value: 1, label: "1", fullStop: true },
  { value: 1/1.3, label: "1/1.3", fullStop: false },
  { value: 1/1.6, label: "1/1.6", fullStop: false },
  { value: 1/2, label: "1/2", fullStop: true },
  { value: 1/2.5, label: "1/2.5", fullStop: false },
  { value: 1/3.2, label: "1/3.2", fullStop: false },
  { value: 1/4, label: "1/4", fullStop: true },
  { value: 1/5, label: "1/5", fullStop: false },
  { value: 1/6, label: "1/6", fullStop: false },
  { value: 1/8, label: "1/8", fullStop: true },
  { value: 1/10, label: "1/10", fullStop: false },
  { value: 1/13, label: "1/13", fullStop: false },
  { value: 1/15, label: "1/15", fullStop: true },
  { value: 1/20, label: "1/20", fullStop: false },
  { value: 1/25, label: "1/25", fullStop: false },
  { value: 1/30, label: "1/30", fullStop: true },
  { value: 1/40, label: "1/40", fullStop: false },
  { value: 1/48, label: "1/48", fullStop: false, class: "recommend" },
  { value: 1/50, label: "1/50", fullStop: false, class: "recommend" },
  { value: 1/60, label: "1/60", fullStop: true, class: "recommend" },
  { value: 1/80, label: "1/80", fullStop: false, class: "recommend" },
  { value: 1/100, label: "1/100", fullStop: false, class: "recommend" },
  { value: 1/125, label: "1/125", fullStop: true, class: "recommend" },
  { value: 1/160, label: "1/160", fullStop: false, class: "recommend" },
  { value: 1/200, label: "1/200", fullStop: false, class: "recommend" },
  { value: 1/250, label: "1/250", fullStop: true, class: "recommend" },
  { value: 1/320, label: "1/320", fullStop: false },
  { value: 1/400, label: "1/400", fullStop: false },
  { value: 1/500, label: "1/500", fullStop: true },
  { value: 1/640, label: "1/640", fullStop: false },
  { value: 1/800, label: "1/800", fullStop: false },
  { value: 1/1000, label: "1/1000", fullStop: true },
  { value: 1/1300, label: "1/1300", fullStop: false },
  { value: 1/1600, label: "1/1600", fullStop: false },
  { value: 1/2000, label: "1/2000", fullStop: true },
  { value: 1/2500, label: "1/2500", fullStop: false },
  { value: 1/3200, label: "1/3200", fullStop: false },
  { value: 1/4000, label: "1/4000", fullStop: true }
];

/**
 * Default to ISO 400
 */
RV_CURRENT_ISO_VALUE = new ReactiveVar(400);

/**
 * Panasonic w/grey card  -> 171
 * Minolta light meter    -> 220
 */
RV_CALIBRATION_CONSTANT = new ReactiveVar(220);

/**
 * Default to 3300
 */
RV_CURRENT_LUX_VALUE = new ReactiveVar(3300);

Template._component.events({
  submit(e) {
    e.preventDefault();
  },
  "change input"(e) {
    RV_CURRENT_LUX_VALUE.set(Number(e.target.value));
    e.preventDefault();
  },
  "change select"(e) {
    RV_CURRENT_ISO_VALUE.set(Number(e.target.options[e.target.selectedIndex].value));
  }
});

Template._component.onRendered(function() {
  this.$("select").dropdown();
});

Template._component.helpers({
  data() {
    return {
      isoValues: ISO_VALUES,
      currentIsoValue: RV_CURRENT_ISO_VALUE.get(),
      currentLuxValue: RV_CURRENT_LUX_VALUE.get()
    };
  },
  isIsoSelected() {
    return this.value === RV_CURRENT_ISO_VALUE.get();
  },
  isNearPerfectExposure() {
    return this.compensation === "0.01";
  },
  exposureTable() {
    const iso = RV_CURRENT_ISO_VALUE.get();
    const calibration = RV_CALIBRATION_CONSTANT.get();
    const lux = RV_CURRENT_LUX_VALUE.get();
    const outputTable = [];
    F_VALUES.forEach(f => {
      const exposureTime = (calibration * f.value * f.value) / (lux * iso);
      const sValueWithRelativeError = S_VALUES.map(s => {
        const e = (s.value - exposureTime) / exposureTime;
        return {
          sValue: s,
          relativeError: e,
          absRelativeError: Math.abs(e)
        };
      });
      const closestSValue = _.min(sValueWithRelativeError, r => r.absRelativeError);
      if (closestSValue.absRelativeError < MAX_ERROR) {
        outputTable.push({
          fValue: f,
          sValue: closestSValue.sValue,
          absRelativeError: closestSValue.absRelativeError,
          relativeError: closestSValue.relativeError,
          isNegative: closestSValue.relativeError < 0,
          compensation: closestSValue.absRelativeError.toFixed(2),
          compensationFontSize: (10 + closestSValue.absRelativeError * 7).toFixed(0)
        });
      }
    });
    return {
      values: outputTable
    };
  }
});
